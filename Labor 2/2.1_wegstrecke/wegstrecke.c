#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define CITIES 6
#define TRENNZEICHEN "-"

const char* cities[CITIES] = {"Wien","Linz","Graz","Salzburg","Innsbruck","Klagenfurt"};

int weg(char* from, char* to);
int getIndex(char* city);

//Retouniert Distanz from zu to; -1 ==> Fehlerhafte Eingabe
int weg(char* from, char* to) {
    const int distance[CITIES][CITIES] = {
        {0, 188, 204, 302, 468, 310},
        {188, 0, 276, 125, 291, 274},
        {204, 276, 0, 278, 444, 152},
        {302, 125, 278, 0, 166, 242},
        {468, 291, 444, 166, 0, 326},
        {310, 274, 152, 242, 326, 0}
    };
    if(getIndex(from)>=0&&getIndex(to)>=0) {
        return distance[getIndex(from)][getIndex(to)];
    } else
        return -1;

}

//Liefert den Index der Stadt; -1 ==> Fehlerhafte Eingabe
int getIndex(char* city) {

    int i;
    for(i=0; i<CITIES; i++) {
        if(strcmp(city,cities[i])==0)
            return i;
    }
    if(strlen(city)==1) {
        for(i=0; i<CITIES; i++) {
            if(city[0]==cities[i][0])
                return i;
        }
    }
    return -1;

}

/*
argc -> ArgumentCount == wieviele Komandozeilenparamenter
argv --> Komandozeilenparameter; argv[0] == "Programmname"
*/
int main(int argc, char* argv[]) {

    int i, sum = 0;
    char *streckenname = calloc(sizeof(char),argc);

    if(argc < 3){
        printf("Es wurden zu wenige Argumente eingegeben.");
        return -1;
    }

    if(getIndex(argv[1])>=0)
        strcpy(streckenname,argv[1]);

    for(i=1;i<argc-1;i++){
        if(weg(argv[i],argv[i+1])<0){
            printf("Fehler! Falscher Ortsname eingegeben!");
            return -1;
        }
        sum += weg(argv[i],argv[i+1]);
        strcat(streckenname,TRENNZEICHEN);
        strcat(streckenname,argv[i+1]);

    }
    printf("Die Strecke %s betraegt %d",streckenname,sum);

    free(streckenname);
    return 0;
}
