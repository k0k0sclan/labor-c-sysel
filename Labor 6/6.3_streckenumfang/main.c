#include <stdio.h>
#include <stdlib.h>
#include <math.h>

typedef struct {
    double x;
    double y;
} punkt_t;

typedef struct {
    punkt_t p1;
    punkt_t p2;
} strecke_t;

double getDistance(strecke_t *s);
double umfang(punkt_t *neck,size_t n);


int main() {
    punkt_t dreieck[3];
    dreieck[0].x=0;
    dreieck[0].y=0;
    dreieck[1].x=3;
    dreieck[1].y=0;
    dreieck[2].x=3;
    dreieck[2].y=4;
    printf("Umfang: %g",umfang(dreieck,3));
    return 0;
}

double getDistance(strecke_t *s) {

    return sqrt(pow(s->p2.x-s->p1.x,2)+pow(s->p2.y-s->p1.y,2));

}

double umfang(punkt_t *neck,size_t n) {

    int i,sum=0;
    strecke_t s;
    for(i=1; i<n;i++) {
        s.p1=neck[i-1];
        s.p2=neck[i];
        sum+=getDistance(&s);
    }
    s.p1=neck[--i];
    s.p2=neck[0];
    sum+=getDistance(&s);
    return sum;
}
