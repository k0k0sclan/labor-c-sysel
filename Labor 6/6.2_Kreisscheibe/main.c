#include <stdio.h>
#include <stdlib.h>
#include <math.h>

typedef struct{
    double x;
    double y;
}punkt_t;

typedef struct{
punkt_t center;
double radius;
}kreis_t;

int getPunkt(punkt_t* p1);
int getKreis(kreis_t* k1);
double getDistance(punkt_t p1,punkt_t p2);

int main()
{

    kreis_t k;
    punkt_t p;

    if(!getKreis(&k))
        return 1;
    printf("\n\n");
    if(!getPunkt(&p))
        return 1;

    if(getDistance(k.center,p)<=k.radius)
        printf("Der Punkt liegt auf dem Kreis");

    return 0;
}

int getPunkt(punkt_t* p){

    int test;
    char c;
    printf("Geben sie die x Koordinate ein -->");
    fflush(stdin);
    test = scanf("%lf%c",&(p->x),&c);
    if(test!=2||c!='\n')
        return 0;
    printf("Geben sie die y Koordinate ein -->");
    fflush(stdin);
    test = scanf("%lf%c",&(p->y),&c);
    if(test!=2||c!='\n')
        return 0;

    return 1;
}

int getKreis(kreis_t* k){

    int test;
    char c;
    printf("Geben sie den Mittelpunkt ein:\n");
    if(!getPunkt(&(k->center)))
        return 0;
    printf("\nGeben sie den Radius ein -->");
    fflush(stdin);
    test = scanf("%lf%c",&(k->radius),&c);
    if(test!=2||c!='\n')
        return 0;

    return 1;
}

double getDistance(punkt_t p1,punkt_t p2){

    return sqrt(pow(p2.x-p1.x,2)+pow(p2.y-p1.y,2));

}
