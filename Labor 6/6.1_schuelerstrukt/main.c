#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#define bigger(c1,c2) (c1)<(c2)? -1: ((c2)<(c1)? 1:0)

typedef struct {
    char zuname[31];
    char vorname[31];
    char klasse[6];
    int noten[4];
} schueler;

double notenschnitt(schueler* sch);
void init(schueler* arr,int n);
int notencmp(const void *v1,const void *v2);
int import(schueler** arr);

int main() {
    srand(time(NULL)*10);
    int i,n;
    schueler* arr=NULL;

    schueler philip = {"Philip","Daxboeck","2CHIF",{2,4,3,1}};

    printf("Der Schueler %s hat einen Notendurchschnitt von %.2lf\n",philip.vorname,notenschnitt(&philip));

    n = import(&arr);

    if(!n)
        return 1;

    qsort(arr,n,sizeof(schueler),notencmp);

    for(i=0; i<n; i++)
        printf("%d. Platz: %s %s %.2lf\n",i+1,arr[i].vorname,arr[i].zuname,notenschnitt(&arr[i]));

    free(arr);
    return 0;
}

double notenschnitt(schueler *sch) {

    int i,sum=0;

    for(i=0; i<4; i++)
        sum += sch->noten[i];

    return (double)sum/4.0;

}


int notencmp(const void *v1,const void *v2) {

    schueler* s1 = (schueler*)v1;
    schueler* s2 = (schueler*)v2;

    double ns1 = notenschnitt(s1);
    double ns2 = notenschnitt(s2);

    return bigger(ns1,ns2);

}

int import(schueler** arr) {

    FILE* fp = fopen("klasse.csv","r");
    char buf[100];
    char* elem;
    int n,i,j;

    if(!fp)
        return 0;

    fscanf(fp,"%d\n",&n);
    *arr = calloc(sizeof(schueler),n);

    for(i=0; i<n; i++) {

        fgets(buf,100,fp);
        elem = strtok(buf,";");
        strcpy((*arr)[i].zuname,elem);
        elem = strtok(NULL,";");
        strcpy((*arr)[i].vorname,elem);
        elem = strtok(NULL,";");
        strcpy((*arr)[i].klasse,elem);

        for(j=0; j<4; j++) {
            elem = strtok(NULL,";");
            sscanf(elem,"%d",&((*arr)[i].noten[j]));
        }
    }

    fclose(fp);
    return n;
}
