#include <stdio.h>
#include <stdlib.h>

int countchar(FILE* fp,char c);

int main()
{
    FILE* fp = fopen("text.txt","r");
    if(!fp)
        return 1;
    char c = 'm';
    printf("Anzahl %c im File: %d",c,countchar(fp,c));
    fclose(fp);
    return 0;
}

int countchar(FILE* fp,char c){

    size_t pos=ftell(fp);
    int sum=0;

    while(!feof(fp)){
        if(fgetc(fp)==c)
            sum++;
    }

    fseek(fp,0,pos);
    return sum;
}
