#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>

#define MAX_LINE 255

int main(int argc,char* argv[]){

  int i=0,sum=0;
  char buf[MAX_LINE];

  system("cls");
  printf("\n\n");

  if(argc!=3){
    printf("Falsche Anzahl an Argumenten!");
    return 1;
  }

  FILE *fp = fopen(argv[1],"r");

  if(!fp){
    printf("Datei nicht gefunden!");
    return 2;
  }

  for(i=0;!feof(fp);i++){
    fgets(buf,MAX_LINE,fp);
    if(strstr(buf,argv[2])){
      sum++;
      printf("%-3d\t%s",i,buf);
    }
  }
  printf("\n\n%d Zeilen gefunden\nTaste druecken...",sum);
  getch();

  fclose(fp);
  return 0;
}
