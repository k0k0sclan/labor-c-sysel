#include <stdio.h>
#include <stdlib.h>

int* zahlenfolge(int z1,int z2);

int main(int argc,char** argv) {

    int *arr;
    int i,z1,z2,n;

    if(argc<3){
        printf("Zu wenige Argumente!");
        return 3;
    }

    if(!sscanf(argv[1],"%d",&z1))
        return 1;

    if(!sscanf(argv[2],"%d",&z2))
        return 1;

    n = z1<z2?z2-z1+1:z1-z2+1;

    arr=zahlenfolge(z1,z2);

    if(arr==NULL)
        return 2;

    for(i=0;i<n;i++)
        printf("%d\n",arr[i]);

    free(arr);
    return 0;
}

int* zahlenfolge(int z1,int z2) {

    int* arr;
    int i;

    if(z1-z2<0) {
        arr = calloc(sizeof(int),z2-z1+1);
        if(arr==NULL)
            return NULL;
        for(i=0; i<=z2-z1; i++)
            arr[i]=i+z1;
    } else if(z1-z2>0) {
        arr = calloc(sizeof(int),z1-z2+1);
        if(arr==NULL)
            return NULL;
        for(i=0; i<=z1-z2; i++)
            arr[i]=z1-i;
    } else {
        arr = calloc(sizeof(int),1);
        if(arr==NULL)
            return NULL;
        arr[0]=z1;
    }

    return arr;
}
