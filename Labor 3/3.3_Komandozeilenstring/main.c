#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char** argv)
{
    size_t n=1;
    int i;
    char* str;

    for(i=1;i<argc;i++)
        n+=strlen(argv[i]);

    str=calloc(sizeof(char),n);

    if(str==NULL)
        return 1;

    for(i=1;i<argc;i++)
        strcat(str,argv[i]);

    puts(str);

    free(str);
    return 0;
}
