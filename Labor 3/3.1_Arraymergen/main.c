#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int* merge(int* arr1,int n1,int* arr2,int n2);
void out(int* arr,int n);
int cmp(const void* z1,const void* z2);

int main( void ) {
  int arr1[] = {3, 7, 5, 3, 9, 1};
  int arr2[] = {1, 2, 7, 4};
  size_t n1 = sizeof( arr1 ) / sizeof( int );
  size_t n2 = sizeof( arr2 ) /   sizeof( int );
  int* erg;

  erg = merge(arr1,n1,arr2,n2);

  if(erg){
    out(erg,n1+n2);
    free(erg);
  }

  else
    return 1;

  return 0;
}

int* merge(int* arr1,int n1,int* arr2,int n2){

    int* arr = calloc(sizeof(int),n1+n2);
    memcpy(arr,arr1,n1*sizeof(int));
    memcpy(arr+n1,arr2,n2*sizeof(int));
    qsort(arr,n1+n2,sizeof(int),cmp);
    return arr;

}

void out(int* arr,int n){

    int i;
    for(i=0;i<n;i++)
        printf("%d ",arr[i]);

}

int cmp(const void* z1,const void* z2){

    int* v1 = (int*)z1;
    int* v2 = (int*)z2;

    if(*v1<*v2)
        return -1;
    else if(*v1==*v2)
        return 0;
    else if(*v1>*v2)
        return 1;

    return 0;

}
