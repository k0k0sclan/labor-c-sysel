#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MIN 0
#define MAX 20

void init(int* arr,size_t n);
int cmp(const void *v1,const void *v2);

int main() {

    srand(time(NULL)*100);

    int *arr = calloc(200,sizeof(int));
    size_t n = 200;
    int i;

    init(arr,n);

    printf("Unsortiert:\n");

    for(i=0; i<n; i++)
            printf("arr[%3d]: %2d\n",i,arr[i]);

    qsort(arr,n,sizeof(int),cmp);

    printf("\n\n\nSortiert:\n");

    for(i=0; i<n; i++)
            printf("arr[%3d]: %2d\n",i,arr[i]);

    return 0;
}


void init(int* arr, size_t n) {

    int i;

    for(i=0; i<n; i++)
        arr[i]=rand()%(MAX+1)+MIN;

}

int cmp(const void *v1,const void *v2) {

    int z1 = *(int*)v1;
    int z2 = *(int*)v2;

    return z1<z2? -1: (z2<z1? 1:0);

}
