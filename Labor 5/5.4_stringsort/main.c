#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define bigger(c1,c2) (c1)<(c2)? -1: ((c2)<(c1)? 1:0)

int charcmp(const void *v1,const void *v2);

int main() {
    char str[] = "a7B(5A)b";

    printf("String vorm Sortieren: %s",str);

    qsort(str,sizeof(str)-sizeof(char),sizeof(char),charcmp);

    printf("\nString nach dem Sortieren: %s",str);

    return 0;
}

int charcmp(const void *v1,const void *v2) {

    char c1 = *(int*)v1;
    char c2 = *(int*)v2;

    if(isalnum(c1)&&isalnum(c2))
        return bigger(c1,c2);
    if(isalnum(c1)&&!isalnum(c2))
        return -1;
    if(!isalnum(c1)&&isalnum(c2))
        return 1;
    return bigger(c1,c2);

}
