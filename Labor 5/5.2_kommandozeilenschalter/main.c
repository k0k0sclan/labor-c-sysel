#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int strcomp(const void *v1, const void *v2);

int main(int argc, char** argv) {

    int i;

    qsort(argv+1,argc-1,sizeof(char*),strcomp);

    for(i=1;i<argc;i++)
        puts(argv[i]);

    return 0;
}

int strcomp(const void *v1, const void *v2){

    char *arr1 = *(char**)v1;
    char *arr2 = *(char**)v2;

    return arr1[0]=='-'&&!(arr2[0]=='-') ? -1: (arr2[0]=='-'&&!(arr1[0]=='-') ? 1:strcmp(arr1,arr2));

}
