#include <stdio.h>
#include <stdlib.h>

int zifsum(int z);
int zifsumcmp(const void* v1,const void* v2);

int main() {

    int arr[] = {17, 123, 33, 8, 99, 6, 204};
    size_t n = sizeof(arr)/sizeof(int);
    int i;

    printf("Array vor dem Sortieren: ");
    for(i=0; i<n; i++)
        printf("%d ",arr[i]);

    qsort(arr,n,sizeof(int),zifsumcmp);

    printf("\n\nArray nach dem Sortieren: ");
    for(i=0; i<n; i++)
        printf("%d ",arr[i]);


    return 0;
}

int zifsum(int z) {

    int sum=0;

    while(z>0) {
        sum += z%10;
        z /= 10;
    }

    return sum;
}

int zifsumcmp(const void *v1, const void *v2) {

    int z1 = zifsum(*(int*)v1);
    int z2 = zifsum(*(int*)v2);
    int x1 = *(int*)v1;
    int x2 = *(int*)v2;

    return z1<z2? -1: (z2<z1? 1: (x1<x2? -1: x2<x1? 1:0));
}
