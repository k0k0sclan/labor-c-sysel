#include <stdio.h>

int main(int argc, char** argv) {

    char c;
    double z1,z2,erg;

    if(argc != 4) {
        printf("Falsche Anzahl Argumente eingegeben!\n");
        return 1;
    }

    if(sscanf(argv[1], "%lf%c", &z1, &c) != 1) {
        printf("Fehler! Keine korrekte Zahl eingegeben!\n");
        return 1;
    }


    if(sscanf(argv[3], "%lf%c", &z2, &c) != 1) {
        printf("Fehler! Keine korrekte Zahl eingegeben!\n");
        return 1;
    }

    switch(argv[2][0]) {
    case '+':
        erg = z1 + z2;
        break;
    case '-':
        erg = z1 - z2;
        break;
    case 'x':
        erg = z1 * z2;
        break;
    case '/':
        if(z2 == 0) {
            printf("Fehler! Division durch Null!");
            return 1;
        }
        erg = z1 / z2;
        break;
    default:
        printf("Falsches Rechenzeichen!");
        return 1;
    }

    printf("%g %c %g ergibt %g\n",z1,argv[2][0],z2,erg);

    return 0;
}
