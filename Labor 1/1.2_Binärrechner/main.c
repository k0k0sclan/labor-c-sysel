#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

int binToInt(char* bin, int* z);
int intToBin(int z, char* bin);


int main(int argc, char** argv) {

    int z1,z2,erg;
    char op;
    char bin[32] = "";

    if(argc != 4) {
        printf("Fehler! Falsche Anzahl Argumente eingegeben!\n");
        return 1;
    }
    op = argv[2][0];

    if(binToInt(argv[1], &z1)|| binToInt(argv[3], &z2)) {
        printf("Falsche Eingabe! Nur 0 und 1 moeglich!\n");
        return 1;
    }


    switch(argv[2][0]) {
    case '+':
        erg = z1 + z2;
        break;
    case '-':
        erg = z1 - z2;
        break;
    case 'x':
        erg = z1 * z2;
        break;
    case '/':
        if(z2 == 0) {
            printf("Fehler! Division durch Null!");
            return 1;
        }
        erg = z1 / z2;
        break;
    default:
        printf("Fehler! Falscher Rechenoperator eingegeben (+ - x /)");
        return 1;
    }



    if(intToBin(erg, bin)) {
        printf("Fehler! Das Ergebnis darf nicht negativ sein!");
        return 1;
    }

    printf("%s %c %s = %s\n%d %c %d = %d",argv[1],op,argv[3],bin,z1,op,z2,erg);

    return 0;
}


int intToBin(int z, char* bin) {

    int i = 0;

    if(z < 0)
        return 1;

    do {

        if(z%2==1)
            bin[i]='1';
        else
            bin[i]='0';

        z /= 2;
        i++;
    } while(z != 0);

    strrev(bin);
    return 0;
}


int binToInt(char* bin, int* z) {
    int n,i,exponent;

    *z = 0;
    n = strlen(bin);

    for(i = 0; i < n; i++)
        if(bin[i] != '0' && bin[i] != '1')
            return 1;


    for(i=0,exponent=n-1;i<n;i++){
        *z += (bin[i] - '0') * pow(2,exponent);
        exponent--;
        }

    return 0;
}
