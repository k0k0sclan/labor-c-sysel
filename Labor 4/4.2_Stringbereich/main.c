#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *makeSubstring(char *text, int von, int bis, int delete);

int main() {

    char str[]="Dies ist ein Test";
    char* str2 = makeSubstring(str,2,7,1);

    if(str2!=NULL) {
        puts(str2);
        puts(str);
        free(str2);
        return 0;
    }

    printf("Fehler!");
    return 1;

}

char *makeSubstring(char *str, int von, int bis, int delete) {

    int i;

    if((von<bis&&von>=0)&&bis<=strlen(str)) {

        char* str2 = calloc(sizeof(char),bis-von+1);

        if(str2==NULL)
            return NULL;

        strncpy(str2,str+von,bis-von);

        if(delete) {
            for(i=von; i<strlen(str)-(bis-von); i++)
                str[i]=str[i+bis-von];
            str[i]='\0';
        }


        return str2;
    }
    return NULL;
}
