#include <stdio.h>
#include <stdlib.h>

int* makeTeilFeld(int *ursprungsFeld, int lang, int von, int bis, int *langNeu);

int main()
{
    int f[] = {1,12,3,23,34,54,44,33,12};
    int i;
    int laengeTeilfeld;
    int *nF = makeTeilFeld(f, 9, 1, 25, &laengeTeilfeld);

    if(nF==NULL)
        return 1;

    for(i=0;i<laengeTeilfeld;i++)
    printf("%d ",nF[i]);

    free(nF);

    return 0;
}

int* makeTeilFeld(int *ursprungsFeld, int lang, int von, int bis, int *langNeu){

    int i,j=0,n=0;

    if(von>bis){
        int h=von;
        von=bis;
        bis=h;
    }

    for(i=0;i<lang;i++){
        if(ursprungsFeld[i]>=von&&ursprungsFeld[i]<=bis)
            n++;
    }

    int* arr = calloc(sizeof(int),n);
    if(arr==NULL)
        return NULL;

    for(i=0;i<lang;i++){
        if(ursprungsFeld[i]>=von&&ursprungsFeld[i]<=bis){
            arr[j++]=ursprungsFeld[i];
        }

    }

    *langNeu=n;
    return arr;
}
